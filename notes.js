// Comparison Query Operators


//$gt/$gte Operator



//db.collectionName.find({ field: { $gt : number value } })
//db.collectionName.find({ field: { $gte : number value } })
db.users.find({ age: { $gt : 50 } })

//$lt/$lte Operator

//db.collectionName.find({ field: { $lt : number value } })
//db.collectionName.find({ field: { $lte : number value } })
db.users.find({ age: { $lt : 76 } })



//$ne operator
//db.collectionName.find({ field: { $ne: value } })
db.users.find({ age: { $ne : 76 } })



//$in operator
//Allows us to find documents with specific match criteria one field using different values
//db.collectionName.find({ field: { $in : value }})
db.users.find({ lastName: { $in : ["Hawking", "Doe"] } })
db.users.find({ courses: { $in : ["Sass", "React"] } })



//Logical Query Operator
//$or operator
//Allows us to find documents that match a single criteria from multiple provided search criteria
//db.collectionName.find({ $or: [{ field: value}, { field: value}] })
db.users.find({ $or: [ {firstName: "Neil"}, {age: 25 } ] })
db.users.find({ $or: [ {firstName: "Neil"}, {age: {$gt : 30} } ] })


//$and Operator
//allows us to find documents matching multiple criteria in a single field

//db.collectionName.find({ $and : [{fieldA: valueA}, {fieldB: valueB}] })
db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 76 }} ] })




//Field Projection


//Inclusion
//Allows us to include/add specific fields only when retrieving documents
//The value can be provided to 1 to denote the field is being included

//db.user.find({criteria} , {field: 1})
db.users.find({ firstName: "Jane" }, { 
   
   firstName: 1,
    lastName: 1,
    contact: 1
   
    })



//Exclusion
//0 detones the exception of the field
db.users.find({ firstName: "Jane" }, { 
   
   firstName: 1,
    lastName: 1,
    contact: 1,
    _id: 0
   
    })



//embedded documents

contact: {
	phone: "83927497"
}


"contact.phone"

//returning specific fields in embedded documents


db.users.find({firstName: "Jane"},{
    
    firstName:1,
    lastName:1,
    contact:{
    	phone: 1
    },
    _id:0
    
    })

db.users.find({firstName: "Jane"},{
    
    firstName:1,
    lastName:1,
    "contact.phone": 1,
    _id:0
    
    })

//field projection and slice operator
//$slice operator allows us to retrieve portions of element that matches the search critera
db.users.find({},{
    
 
    _id: 0,
    courses: { $slice: [1,2] }

    
    })



//Evaluation Query Operators
//$regex operator
//allows us to find documents that match a specific string pattern using regular expressions
//db.users.find({ field: {$regex: 'pattern', $options: '$i'} })

//case sensitive query
db.users.find({  firstName: { $regex: 'N'}  })
db.users.find({  firstName: { $regex: 'j'}  })

//case insensitive query
db.users.find({  firstName: { $regex: 'j', $options: '$i'}  })
//here $options with '$i' value specifies that we want to carry out search without considering upper or lowercase.

